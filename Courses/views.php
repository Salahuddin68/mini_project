<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=course;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` ORDER BY id DESC";

//execute the query using php

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="./boot/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
   table {
  border-collapse: separate;
  border-spacing: 4px;

}
table,
th,
td {
  border: 1px solid #cecfd5;
}
th,
td {
  padding: 10px 15px;
}
</style>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div><a href="index.php"> Add New </a></div>
            <table class="table table-bordered">
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>Subject Code</th>
                       <th>Subject Title</th>
                       <th>Department</th>
                       <th>Created At</th>
                       <th>Modiefied At</th>
                       <th>Actions</th>
                   </tr>
               </thead>
                <tbody>
                <?php

                //foreach ($db->query($query) as $row) {
                foreach ($db->query($query) as $students): ?>

                    <tr>
                        <td><?php echo $students['id'];?></td>
                        <td><?php echo $students['sub_code'];?></td>
                        <td><?php echo $students['sub_title'];?></td>
                        <td><?php echo $students['dept'];?></td>
                        <td><?php echo date("d/m/Y",strtotime($students['created_at']));?></td>
                        <td><?php echo $students['modified_at'];?></td>
                        <td> <a  href="show.php?id=<?php echo $students['id'];?>" class="btn btn-primary">Show</a>
                            | <a href="edit.php?id=<?php echo $students['id'];?>" class="btn btn-warning">Edit</a> |
                            <a href="delete.php?id=<?php echo $students['id'];?>" class="btn btn-danger">Delete</a></td>

                    </tr>
                <?php
                endforeach;
                //}
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>

